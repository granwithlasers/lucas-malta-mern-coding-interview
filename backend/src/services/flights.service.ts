import { StatusInput } from '../controllers/flights.controller'
import { FlightsModel } from '../models/flights.model'

export class FlightsService {
    async getAll() {
        return await FlightsModel.find()
    }

    async updateStatus(statusInput:StatusInput) {
        return await FlightsModel.updateOne({ '_id': statusInput.uuid }, { status: statusInput.newStatus })
    }
}
