import React, { FC, useEffect, useState } from "react";
import { Box, Card, Container, Typography, Select, FormControl, InputLabel } from "@material-ui/core";

import { FlightStatuses } from "../../models/flight.model";
import { BackendClient } from "../../clients/backend.client";

interface FlightCardProps {
  id: string;
  code: string;
  origin: string;
  destination: string;
  passengers?: string[];
  status: FlightStatuses;
}

const mapFlightStatusToColor = (status: FlightStatuses) => {
  const mappings = {
    [FlightStatuses.Arrived]: "#1ac400",
    [FlightStatuses.Delayed]: "##c45800",
    [FlightStatuses["On Time"]]: "#1ac400",
    [FlightStatuses.Landing]: "#1ac400",
    [FlightStatuses.Cancelled]: "#ff2600",
  };

  return mappings[status] || "#000000";
};

const backendClient = BackendClient.getInstance()

export const FlightCard: React.FC<FlightCardProps> = (
  props: FlightCardProps
) => {

  const [status, setStatus] = useState(props.status);

  useEffect(() => {
    // console.log(fromprops.status)
    if(status === props.status) return
    backendClient.updateStatus({uuid: props.id, newStatus: status})
  }, [status, props])

  return (
    <Card
      style={{
        backgroundColor: "#f5f5f5",
        margin: "15px",
        padding: "35px",
        justifyContent: "center",
      }}
    >
      <Box style={{ display: "flex", justifyContent: "space-between" }}>
        <Typography variant="h5">{props.code} </Typography>
        {/* <Typography style={{ color: mapFlightStatusToColor(props.status) }}>
          Status: {props.status}
        </Typography> */}
      <FormControl>
          <InputLabel htmlFor="age-native-simple">Status</InputLabel>
          <Select
            native
            value={status}
            onChange={(e) => setStatus(e.target.value as any)}
          >
            <option aria-label="None" value="" />
            <option value={'Arrived'}>Arrived</option>
            <option value={'Cancelled'}>Cancelled</option>
            <option value={'On Time'}>On Time</option>
            <option value={'Landing'}>Landing</option>
            <option value={'Delayed'}>Delayed</option>
          </Select>
        </FormControl>
      </Box>

      <Box>
        <Typography>Origin: {props.origin}</Typography>
      </Box>
      <Box>
        <Typography>Destination: {props.destination}</Typography>
      </Box>
    </Card>
  );
};
